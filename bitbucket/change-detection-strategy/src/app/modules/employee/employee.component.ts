import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css'],
})
export class EmployeeComponent implements OnInit {
  
  pay: any;
  
  constructor() { }

  ngOnInit() {
    this.changeUserToJoe();
  }
  
  doublePay(): void {
    this.pay.count *= 2;
  }
  
  changeUserToJoe() {
    this.pay = {
      count: 100,
      currency: 'pln'
    }
  }
  changeUserToBill() {
    this.pay = {
      count: 20,
      currency: 'euro'
    }
  }

}
