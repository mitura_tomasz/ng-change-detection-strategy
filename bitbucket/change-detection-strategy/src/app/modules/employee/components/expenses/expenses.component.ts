import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-expenses',
  templateUrl: './expenses.component.html',
  styleUrls: ['./expenses.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,

})
export class ExpensesComponent implements OnInit {
  
  @Input() pay: any;
  
  constructor() { }

  ngOnInit() {
  }
  
  doublePay(): void {
    this.pay.count *= 2;
  }
  
}
