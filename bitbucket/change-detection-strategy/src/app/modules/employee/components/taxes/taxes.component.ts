import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-taxes',
  templateUrl: './taxes.component.html',
  styleUrls: ['./taxes.component.css'],
  changeDetection: ChangeDetectionStrategy.Default,

})
export class TaxesComponent implements OnInit {

  @Input() pay: any;
  
  constructor() { }

  ngOnInit() {
  }
  
  doublePay(): void {
    this.pay.count *= 2;
  }

}
