import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeComponent } from './employee.component';
import { TaxesComponent } from './components/taxes/taxes.component';
import { ExpensesComponent } from './components/expenses/expenses.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    EmployeeComponent,
    TaxesComponent,
    ExpensesComponent
  ],
  exports: [
    EmployeeComponent
  ]
})
export class EmployeeModule { }
